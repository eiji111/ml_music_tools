# coding: utf-8

import music21
import numpy as np
import matplotlib.pyplot as plt

import pianoroll
import ml_midi

def plot_mat(matrix):
    if len(matrix.shape) != 2:
        print("Invalid shape: {}. Shape should be 2 dim matrix.".format(matrix.shape))
        print(matrix)
        return
    plt.imshow(matrix)
    plt.show()

def plot(obj):
    if isinstance(obj, str):
        score = music21.converter.parse(obj)
        return score.plot('pianoroll')
    elif isinstance(obj, ml_midi.NoteSequence):
        temp_midi = "temp_midi_plot__.mid"
        obj.save_as_file(temp_midi)
        score = music21.converter.parse(temp_midi)
        return score.plot('pianoroll')
    elif isinstance(obj, pianoroll.Pianoroll):
        plot_mat(np.flip(obj.encoded_matrix.T,axis=0))
    elif isinstance(obj, np.ndarray):
        plot_mat(obj)
