# coding: utf-8

import pretty_midi
import numpy as np
import os # file io
import pianoroll as pr
from pianoroll import kNoNote, kNoteOn, kNoteArticulate, kNoteOff, kNoteOnAndOff

def check_midifile(midi_filepath, plot=False):
    midi_data = pretty_midi.PrettyMIDI(midi_filepath)
    print("timebase: {}".format(midi_data.resolution))
    print("time_signature_changes: {}".format(midi_data.time_signature_changes))
    print("key_signature_changes: {}".format(midi_data.key_signature_changes))

    tempo = midi_data.estimate_tempo()
    print("estimated tempo: {}".format(tempo))
    tempi = midi_data.estimate_tempi()
    print("estimated tempi: {}".format(tempi))
    tempo_changed_time, changed_tempo = midi_data.get_tempo_changes()
    print("Tempo change time list [sec]: {}".format(tempo_changed_time))
    print("Changed tempo list [bpm]: {}".format(changed_tempo))

    beat_start = midi_data.estimate_beat_start()
    print("estimated beat start [sec]: {}".format(beat_start))
    print("estimated beat start [tick]: {}".format(midi_data.time_to_tick(beat_start)))
    print("estimated beats [sec]: {}".format(midi_data.get_beats()))
    print("estimated beats [tick]: {}".format([midi_data.time_to_tick(event) for event in midi_data.get_beats()]))
    print("downbeats [sec]: {}".format(midi_data.get_downbeats()))
    print("downbeats [tick]: {}".format([midi_data.time_to_tick(event) for event in midi_data.get_downbeats()]))

    print("instruments:{}".format(midi_data.instruments))

    print("onsets for each instruments [sec]: {}".format(midi_data.get_onsets()))
    print("onsets for each instruments [tick]: {}".format([midi_data.time_to_tick(event) for event in midi_data.get_onsets()]))

    print("end time [sec]: {}".format(midi_data.get_end_time()))
    print("end time [tick]: {}".format(midi_data.time_to_tick(midi_data.get_end_time())))

    fs = 100.0
    piano_roll = midi_data.get_piano_roll(fs=fs, times=np.arange(0, midi_data.get_end_time(), 1./fs))
    print("piano roll:")
    print(piano_roll)
    return midi_data

"""
    A class which stores information of note sequence quantization.
    On quantizing sequences, the resolution means how mant segments one beat is divided. (beat_resolution)
    quantized_beat_ticks indicates what ticks corrsponds to beats.
    unit_ticks means the length of one segment/unit of divided beats in ticks.
    |<------------...                bar                              ...----------->|
    |<-------------beat------------------------>|<---------beat ....  ...----beat--->|
    |<--unit-->|<--unit-->|<--unit-->|<--unit-->|
    :todo: Define Note class in the future.
"""
class Quantization:
    def __init__(self, beat_resolution, unit_ticks, quantized_beat_ticks):
        self.beat_resolution = beat_resolution
        self.quantized_beat_ticks = quantized_beat_ticks
        self.unit_ticks = unit_ticks

    def get_total_units(self):
        return len(self.quantized_beat_ticks)

    def __str__(self):
        st = "resolution: {} of 1 beat\n".format(1./self.beat_resolution)
        st += "unit length in ticks: {}\n".format(self.unit_ticks)
        st += "quantized ticks on beats: {}".format(self.quantized_beat_ticks)
        return st

    def to_dict(self):
        return {'beat_resolution':self.beat_resolution, \
                'quantized_beat_ticks':self.quantized_beat_ticks, \
                'unit_ticks':self.unit_ticks}

"""
    Tempo class
"""
class TimeSignature:
    def __init__(self, numerator, denominator, time):
        self.numerator = numerator
        self.denominator = denominator
        self.time = time

    def __str__(self):
        st = "numerator: {}, ".format(self.numerator)
        st += "denominator: {}, ".format(self.denominator)
        st += "time: {}".format(self.time)
        return st

    def to_dict(self):
        return {'numerator':self.numerator, 'denominator':self.denominator, 'time': self.time}


"""
    Tempo class
"""
class Tempo:
    def __init__(self, bpm, timebase):
        self.bpm = bpm
        self.timebase = timebase

    def __str__(self):
        st = "bpm: {:.3f}\n".format(self.bpm)
        st += "timebase: {}".format(self.timebase)
        return st

    def to_dict(self):
        return {'bpm':self.bpm, 'timebase':self.timebase}

    def get_timebase(self):
        return self.timebase

    def get_bpm(self):
        return self.bpm

class Note:
    num_ids = 0

    def __init__(self, pitch, start_sec, end_sec, velocity, start_tick, end_tick):
        Note.num_ids += 1
        self.id = Note.num_ids
        self.start_sec = start_sec
        self.end_sec = end_sec
        self.start_tick = start_tick
        self.end_tick = end_tick
        self.pitch = int(pitch)
        self.velocity = int(velocity)

    def __str__(self):
        st = "id: {} \n".format(self.id)
        st += "start : {:.3f}, ".format(self.start_sec)
        st += "end [sec]: {:.3f}, ".format(self.end_sec)
        st += "duration [sec]: {:.3f} \n".format(self.end_sec - self.start_sec)
        st += "start : {}, ".format(self.start_tick)
        st += "end [tick]: {}\n".format(self.end_tick)
        st += "pitch: {}, ".format(self.pitch)
        st += "velocity: {}".format(self.velocity)
        return st

    def to_dict(self):
        note = {'id': self.id, \
                'pitch':self.pitch, \
                'start':self.start_sec, \
                'end':self.end_sec, \
                'start_ticks':self.start_tick, \
                'end_ticks':self.end_tick, \
                'velocity':self.velocity}
        return note

"""
    Note sequence class
    :todo: Define Note class in the future.
"""
class NoteSequence:
    def __init__(self):
        self.midi_seq = None
        self.end_time_sec = None
        self.tempo = None
        self.time_signature = None
        self.quantization = None # how to quantized this midi sequence
        self.filepath = None
        self.clear()

    def __str__(self):
        st = "Note sequence:\n"
        st += "tempo: \n"
        if self.tempo is not None:
            st += self.tempo.__str__()
            st += "\n"
        else:
            st += "No tempo is set."
        st += "\n"
        st += "time signature: \n"
        if self.time_signature is not None:
            st += self.time_signature.__str__()
            st += "\n"
        else:
            st += "No time signature is set."
        st += "\n"
        st += "quantize: \n"
        if self.quantization is not None:
            st += self.quantization.__str__()
        else:
            st += "Not quantized."
        st += "\n"
        st += "notes: \n"
        for ind, note in enumerate(self.midi_seq):
            st += "{}th note:\n".format(ind)
            st += note.__str__()
            st += "\n"
        st += "\n"
        return st

    def to_dict(self):
        seq = {'notes':[]}
        for note in self.get_notes():
            seq['notes'].append(note.to_dict())
        if self.tempo is not None:
            seq['tempo'] = self.tempo.to_dict()
        if self.end_time_sec is not None:
            seq['end_time_sec'] = self.end_time_sec
        if self.time_signature is not None:
            seq['time_signature'] = self.time_signature.to_dict()
        if self.quantization is not None:
            seq['quantize'] = self.quantization.to_dict()
        if self.filepath is not None:
            seq['filepath'] = self.filepath
        return seq

    """
        Clear internal sequences
        :param: None
        :rtype: NoteSequence.
        :return: This NoteSequence.
    """
    def clear(self):
        self.midi_seq = []
        self.end_time_sec = None
        self.tempo = None
        self.time_signature = None
        self.quantization = None
        self.filepath = None
        return self

    """
        Deep copy this instance.
        :param: None
        :rtype: NoteSequence.
        :return: Copied NoteSequence.
    """
    def copy(self):
        import copy
        copied = NoteSequence()
        copied = copy.deepcopy(self)
        #copied.midi_seq = copy.deepcopy(self.midi_seq)
        #copied.tempo = copy.deepcopy(self.tempo)
        #copied.quantization = copy.deepcopy(self.quantization)
        return copied

    """
        Add new note
        :param int pitch: pitch of new note [0-127], 60 = C3.
        :param float start_sec: start time of note on event in seconds
        :param float end_sec: end time of note off event in seconds
        :param int velocity: note velocity [0-127]
        :param int start_tick: start time of note on event in ticks
        :param int end_tick: end time of note off event in ticks
        :param bool verbose: True: output debug log
        :rtype: NoteSequence.
        :return: This NoteSequence.
        :todo: Change note type from dict type to class.
    """
    def add_note(self, pitch, start_sec, end_sec, velocity, start_tick, end_tick, verbose=False):
        new_note = Note(pitch, start_sec, end_sec, velocity, start_tick, end_tick)
        if verbose: print(new_note)
        self.midi_seq.append(new_note)
        return self

    # todo: insersion for binary search
    def find_note(self, id):
        def bsearch(array, id):
            left, right = 0, len(array)-1
            found = False
            while left<=right and not found:
                ind = 0
                mid = (left+right)//2
                if array[mid].id == id:
                    ind = mid
                    found = True
                else:
                    if id < array[mid].id:
                        right = mid-1
                    else:
                        left = mid+1
            return ind, found

        array = self.midi_seq
        ind, found = bsearch(array, id)
        if not found:
            return None
        return array[ind]

    def set_meter(self, numerator, denominator, time):
        self.time_signature = TimeSignature(numerator, denominator, time)
        return self

    def set_end_time(self, end_time_sec):
        self.end_time_sec = end_time_sec
        return self

    def set_end_time_tick(self, end_time_tick):
        return self

    def set_midi_filepath(self, filepath):
        self.filepath = filepath
        return self

    def set_tempo(self, bpm, timebase):
        self.tempo = Tempo(bpm, timebase)
        return self

    def get_tempo(self):
        return self.tempo

    def sec_to_tick(self, seconds):
        ticks = sec_to_tick(seconds, self.tempo.timebase, self.tempo.bpm)
        return ticks

    def tick_to_sec(self, ticks):
        seconds = tick_to_sec(ticks, self.tempo.timebase, self.tempo.bpm)
        return seconds

    def get_notes(self):
        return self.midi_seq

    def get_end_time(self):
        return self.end_time_sec

    def get_end_time_tick(self):
        end_time_tick = sec_to_tick(self.end_time_sec, self.tempo.timebase, self.tempo.bpm)
        return end_time_tick

    def get_quantized_ticks(self, beat_resolution):
        tempo = self.get_tempo()
        end_time_tick = self.get_end_time_tick()
        timebase = tempo.get_timebase()
        bpm = tempo.get_bpm()
        # calc quantized ticks
        unit_ticks = float(timebase / float(beat_resolution))
        total_units = int(end_time_tick/unit_ticks + 1)
        return np.array([ int(i * unit_ticks) for i in range(total_units+1) ])

    def get_pitches(self, mode='range'):
        pitches = set()
        for note in self.get_notes():
            pitches.add(note.pitch)
        if mode=='set':
            return list(pitches)
        elif mode=='range':
            pitches = range(min(pitches),max(pitches)+1)
        else:
            pitches = range(min(pitches),max(pitches)+1)
        return list(pitches)

    """
        quantize midi sequence
        :param float bpm: BPM.
        :param float beat_resolution: resolution. divide a beat into 4 segments if beat_resolution = 4.
        :param bool verbose: output log.
        :rtype: NoteSequence.
        :return: This NoteSequence.
        :todo:
    """
    def quantize(self, beat_resolution=4., verbose=False):

        def find_closest(A, target):
            idx = A.searchsorted(target)
            idx = np.clip(idx, 1, len(A)-1)
            idx -= target - A[idx-1] < A[idx] - target
            return idx
            # http://stackoverflow.com/questions/8914491/finding-the-nearest-value-and-return-the-index-of-array-in-python

        def make_quantized_beat_ticks_with_adjusting_pow2_length(unit_ticks, end_tick):
            total_units = int(np.power(2, np.ceil(np.log2(end_tick/unit_ticks+1))))
            #print "timebase {0}, resolution {1}, unit_tick {2}".format(timebase, resolution, unit_ticks)
            return np.array([ i * unit_ticks for i in range(total_units) ])

        midi_events = self.get_notes()
        last_midi_event = midi_events[-1]
        end_time_tick = self.get_end_time_tick()
        tempo = self.get_tempo()
        timebase = tempo.get_timebase()
        bpm = tempo.get_bpm()

        # calc quantized ticks
        unit_ticks = int(timebase / beat_resolution)
        quantized_beat_ticks = make_quantized_beat_ticks_with_adjusting_pow2_length(unit_ticks, end_time_tick)
        if verbose: print(quantized_beat_ticks)
        for note in midi_events:
            # get start / end time
            start = note.start_sec
            end = note.end_sec
            start_ticks = note.start_tick
            end_ticks = note.end_tick
            # quantize
            idx = find_closest(quantized_beat_ticks, start_ticks)
            quantized_start_ticks = quantized_beat_ticks[idx]
            quantized_end_ticks = quantized_start_ticks + (end_ticks - start_ticks)
            quantized_start = tick_to_sec(quantized_start_ticks, timebase, bpm)
            quantized_end = quantized_start + (end - start)

            # overwrite values by quantized time
            note.start_tick = quantized_start_ticks
            note.end_tick = quantized_end_ticks
            note.start = quantized_start
            note.end = quantized_end
            if verbose: print(note)

        # save quantize info
        self.quantization = Quantization(beat_resolution, unit_ticks, quantized_beat_ticks)
        return self

    def get_quantization(self):
        return self.quantization

    """
        Load midi file and convert to note sequence
        :param str filepath: Midi file path.
        :param bool verbose: output debug log if True.
        :rtype: bool.
        :return: Success/Failure.
    """
    def load_from_file(self, filepath, verbose=False):
        if not os.path.exists(filepath):
            print("Error: no file exists {}".format(filepath))
            return False

        # print outline
        if verbose: check_midifile(filepath)

        # load midi file
        midi_data = pretty_midi.PrettyMIDI(filepath)
        if len(midi_data.instruments) == 0:
            print("Error: no instrument track exists.")
            return False

        track = midi_data.instruments[0]
        end_time_sec = midi_data.get_end_time()
        end_time_tick = midi_data.time_to_tick(end_time_sec)

        # get bpm
        def get_bpm(midi_data):
            # todo compare estimated bpm and changed bpm
            # estimated_bpm = midi_data.estimate_tempo()
            tempo_changes_sec, changed_tempos = midi_data.get_tempo_changes()
            if len(changed_tempos) > 0:
                changed_bpm = changed_tempos[0]
            else:
                print("Error: no tempo information is included.")
            return changed_bpm

        bpm = get_bpm(midi_data)

        # get time signature
        time_signature_changes = midi_data.time_signature_changes
        if len(midi_data.time_signature_changes) > 0:
            time_signature = midi_data.time_signature_changes[0]
            if verbose: print(time_signature)
            self.set_meter(time_signature.numerator, time_signature.denominator, time_signature.time)
        else:
            # set default value
            self.set_meter(4, 4, 0.0)

        # get notes
        if len(track.notes) == 0:
            print("Warning: no notes exists.")
            return True

        def get_beat_start(midi_data):
            estimated_beat_start = midi_data.estimate_beat_start()
            if len(track.notes) > 0:
                beat_start = track.notes[0].start
            else:
                beat_start = estimated_beat_start
            # todo compare estimated_beat_start
            return beat_start
        #beat_start = get_beat_start(midi_data)

        midi_events = []
        for note in track.notes:
            if verbose: print(note)
            start_tick = midi_data.time_to_tick(note.start)
            end_tick = midi_data.time_to_tick(note.end)
            self.add_note(note.pitch, note.start, note.end, note.velocity, start_tick, end_tick)

        self.set_end_time(end_time_sec)
        self.set_end_time_tick(end_time_tick)
        self.set_midi_filepath(filepath)
        self.set_tempo(bpm, midi_data.resolution)
        return True

    """
        Save this note sequence as midi file.
        :param str filepath: Midi file path.
        :rtype: NoteSequence.
        :return: This NoteSequence.
        :todo: multitrack
    """
    def save_as_file(self, filepath):
        if  self.get_tempo() is None:
            midi_data = pretty_midi.PrettyMIDI()
        else:
            timebase = self.get_tempo().get_timebase()
            bpm = self.get_tempo().get_bpm()
            midi_data = pretty_midi.PrettyMIDI(resolution=timebase, initial_tempo=bpm)

        program = pretty_midi.instrument_name_to_program('Acoustic Grand Piano')
        instrument = pretty_midi.Instrument(program=program, is_drum=True, name="drum")

        for note in self.get_notes():
            start = note.start_sec
            end = note.end_sec
            pitch = note.pitch
            velocity = note.velocity

            offset = 0.5 # mido error on writing a note at 0 sec
            start += offset
            end += offset
            new_note = pretty_midi.Note(velocity=velocity, pitch=pitch, start=start, end=end)
            instrument.notes.append(new_note)

        midi_data.instruments.append(instrument)
        midi_data.write(filepath)
        return True

    def to_pianoroll(self, beat_resolution=4, ticks=None, pitches=None, encoder=None, verbose=False):
        if ticks is None:
            ticks = self.get_quantized_ticks(beat_resolution)
            if verbose: print("ticks: {}, len = {}".format(ticks, len(ticks)))
        if pitches is None:
            pitches = self.get_pitches()
            if verbose: print("pitches: {}, len = {}".format(pitches, len(pitches)))
        pianoroll = pr.Pianoroll()
        pianoroll.from_note_seq(self, ticks, pitches)
        if encoder is not None:
            pianoroll.encode(encoder)
        return pianoroll


#--------------------------------------------------------------#
# noteseq - midifile conversion functions

def midifile_to_noteseq(midi_filepath, verbose = False):
    note_sequence = NoteSequence()
    note_sequence.load_from_file(midi_filepath)
    return note_sequence

#----------------------------------------------------------#

def noteseq_to_midifile(note_sequence, midi_filepath):
    note_sequence.save_as_file(midi_filepath)

#------------------------------------------------------------------
# conversion functions

def get_ticks_per_sec(timebase, bpm):
    ticks_per_beat = timebase #
    sec_per_beat = 60. / bpm
    ticks_per_sec = ticks_per_beat / sec_per_beat
    return ticks_per_sec

def sec_to_tick(seconds, timebase, bpm):
    ticks_per_sec = get_ticks_per_sec(timebase, bpm)
    ticks = int(ticks_per_sec * seconds)
    return ticks

def tick_to_sec(ticks, timebase, bpm):
    ticks_per_sec = get_ticks_per_sec(timebase, bpm)
    seconds = ticks / ticks_per_sec
    return seconds

def beat_to_measure(beats, meter):
    numerator = meter['numerator']
    denominato = meter['denominato']
    measures = beats / int(numerator)
    remainder_beats = beats % int(numerator)
    return measures, remainder_beats

def measure_to_beat(measures, meter):
    numerator = meter['numerator']
    denominator = meter['denominato']
    beats = numerator * measures
    return beats

def tick_to_beat_measure(ticks, timebase, meter):
    denominator = meter['denominato']
    ticks_per_beat = int(timebase * (4. / float(denominator)))
    beats = ticks / ticks_per_beat
    remainder_ticks = ticks % ticks_per_beat
    measures, remainder_beats = beat_to_measure(beats, meter)
    return measures, remainder_beats, remainder_ticks

def beat_measure_to_tick(measures, beats, timebase, meter):
    ticks_per_beat = int(timebase * (4. / float(denominator)))
    beats = measure_to_beat(measures, meter)
    ticks = ticks_per_beat * beats
    return ticks

def beat_measure_to_second(measures, beats, timebase, meter, bpm):
    ticks = beat_measure_to_tick(measures, beats, timebase, meter)
    seconds = tick_to_sec(ticks, timebase, bpm)
    return seconds

def second_to_beat_measure(seconds, timebase, meter, bpm):
    ticks = sec_to_tick(seconds, timebase, bpm)
    tick_to_beat_measure(ticks, timebase, meter)
    return seconds
