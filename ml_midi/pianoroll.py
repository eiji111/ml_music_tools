# coding: utf-8
import numpy as np
import ml_midi as md

#----------------------------------------------------------#
kNoNote = 0
kNoteOn = 1
kNoteArticulate = 2
kNoteOff = 3
kNoteOnAndOff = 4


# default encoder
def default_midi_encoder(note, event_type):
    if event_type == kNoteOn:
        encoded_val = 1
    elif event_type == kNoteArticulate:
        encoded_val = 2
    elif event_type == kNoteOff:
        encoded_val = 3
    elif event_type == kNoteOnAndOff:
        encoded_val = 4
    else:
        encoded_val = 0
    return encoded_val, True


class PianorollNoteSeqTransform:
    def __init__(self, ticks, pitches):
        self.ticks = ticks
        self.pitches = pitches

    def __str__(self):
        st = ""
        st += "ticks: {}, len = {}\n".format(self.ticks, len(self.ticks))
        st += "pitches: {}, len = {}".format(self.pitches, len(self.pitches))
        return st

    def tickToTimeIndex(self, tick):
        return np.searchsorted(self.ticks, tick)

    def timeIndexToTick(self, time_index):
        return self.ticks[time_index]

    def pitchToPitchIndex(self, pitch):
        return self.pitches.index(pitch)

    def pitchIndexToPitch(self, pitch_index):
        return self.pitches[pitch_index]

class Pianoroll:
    def __init__(self):
        self.note_seq = None
        self.transform = None
        self.matrix = None # store note event matrix
        self.encoded_matrix = None
        self.clear()

    def __str__(self):
        st = ""
        st += "transform: \n"
        st += "{} \n".format(self.transform)

        seq_length = self.encoded_matrix.shape[0]
        pitch_length = self.encoded_matrix.shape[1]
        for time_index in range(seq_length):
            st += "[\n"
            for pitch_index in range(pitch_length):
                encoded_data = self.encoded_matrix[time_index][pitch_index]
                st += "\t{} ".format(encoded_data)
            st += "]\n"
        return st

    def clear(self):
        self.note_seq = None
        self.transform = None
        self.matrix = None
        self.encoded_matrix = None

    """
        Deep copy this instance.
        :param: None
        :rtype: NoteSequence.
        :return: Copied NoteSequence.
    """
    def copy(self):
        import copy
        copied = Pianoroll()
        copied.note_seq = self.note_seq # shallow
        copied.transform = self.transform # shallow
        copied.matrix = copy.deepcopy(self.matrix) # deep
        copied.encoded_matrix = copy.deepcopy(self.encoded_matrix) # deep
        return copied

    def get_matrix(self):
        return self.encoded_matrix

    def get_pitches(self):
        return self.transform.pitches

    def get_ticks(self):
        return self.transform.ticks

    def encode(self, encoder=default_midi_encoder):
        # make matrix with initial value
        original_mat = self.matrix

        seq_length = original_mat.shape[0]
        pitch_length = original_mat.shape[1]

        encoded_data, is_encode = encoder(None, kNoNote)
        encoded_data = np.array([encoded_data])
        if len(encoded_data.shape) == 1:
            encoded_mat = np.zeros((seq_length, pitch_length))
        else:
            encoded_mat = np.zeros((seq_length, pitch_length, encoded_data.shape[0], encoded_data.shape[1]))

        # converted to custom encoding matrix
        for time_index in range(seq_length):
            for pitch_index in range(pitch_length):
                enent_type, note_id = original_mat[time_index][pitch_index]
                note = self.note_seq.find_note(note_id)
                encoded_data, is_encode = encoder(note, enent_type)
                if is_encode:
                    encoded_data = np.array([encoded_data])
                    encoded_mat[time_index][pitch_index] = encoded_data

        self.encoded_matrix = encoded_mat
        return self

    #def decode(self, matrix):
    #    # todo convert matrix to note event matrix
    #    pass

    def to_note_seq(self, matrix):
        # check encoding finished
        has_been_encoded = (self.transform is not None)
        if not has_been_encoded:
            return None

        # todo: use decode matrix directly now

        out_note_seq = md.NoteSequence()
        out_note_seq.tempo = self.note_seq.tempo
        out_note_seq.time_signature = self.note_seq.time_signature
        out_note_seq.quantization = self.note_seq.quantization

        # converted custom encoding matrix to note sequence
        for time_index in range(matrix.shape[0]):
            for pitch_index in range(matrix.shape[1]):
                encoded_data = matrix[time_index][pitch_index]
                is_decode = (encoded_data==1)
                if is_decode:
                    velocity = 60
                    pitch = self.transform.pitchIndexToPitch(pitch_index)
                    start_tick = self.transform.timeIndexToTick(time_index)
                    end_tick = self.transform.timeIndexToTick(time_index+1)
                    start = self.note_seq.tick_to_sec(start_tick)
                    end = self.note_seq.tick_to_sec(end_tick)
                    out_note_seq.add_note(pitch, start, end, velocity, start_tick, end_tick)

        # set end time
        notes = out_note_seq.get_notes()
        last_note = notes[-1]
        out_note_seq.end_time_sec = out_note_seq.tick_to_sec(last_note.end_sec)
        return out_note_seq


    def _makeNoteEventMatrixFromNoteSeq(self, note_seq, coordTransform):
        #----------------------------time-----------#
        #  [1:note on, note id] [2: articulate, note id] [3:note off, note id]
        #
        #
        # pitches
        #
        # internal encoder
        def _note_event_encoder(note, event_type):
            encoded_data = np.array([kNoNote, 0])
            is_encode = True
            if event_type == kNoteOn:
                encoded_data, is_encode = np.array([kNoteOn, note.id]), True
            elif event_type == kNoteArticulate:
                encoded_data, is_encode = np.array([kNoteArticulate, note.id]), True
            elif event_type == kNoteOff:
                encoded_data, is_encode = np.array([kNoteOff, note.id]), True
            elif event_type == kNoteOnAndOff:
                encoded_data, is_encode = np.array([kNoteOnAndOff, note.id]), True
            else:
                encoded_data, is_encode = np.array([kNoNote, 0]), True
            return encoded_data, is_encode

        encoder = _note_event_encoder

        # make matrix with initial value
        seq_length = len(coordTransform.ticks)
        pitch_length = len(coordTransform.pitches)
        initial_value, is_encode = encoder(None, kNoNote)
        midi_mat = np.full((seq_length, pitch_length, initial_value.shape[0]), initial_value)

        for note in note_seq.get_notes():
            # calc start time index
            start_tick = note.start_tick
            start_time_index = coordTransform.tickToTimeIndex(start_tick)
            if start_time_index < 0 or start_time_index >= seq_length:
                print("Warning: start tick is out of range")
                continue
            # calc end time index
            end_tick = note.end_tick
            end_time_index = coordTransform.tickToTimeIndex(end_tick)
            if end_time_index < 0 or end_time_index >= seq_length:
                print("Warning: end tick is out of range {} {}".format(end_time_index, seq_length))
                continue
            # calc pitch index
            pitch = note.pitch
            pitch_index = coordTransform.pitchToPitchIndex(pitch)
            if pitch_index is None:
                print("Warning: pitch is out of range")
                continue

            # encode notes to matrix
            for time_ind in range(start_time_index, end_time_index+1):
                if time_ind == start_time_index and time_ind == end_time_index: # rouch resolution
                    event_type = kNoteOnAndOff
                else:
                    if time_ind == start_time_index:
                        event_type = kNoteOn
                    elif time_ind == end_time_index:
                        event_type = kNoteOff
                    elif time_ind > start_time_index and time_ind < end_time_index:
                        event_type = kNoteArticulate
                    else:
                        event_type = kNoNote
                encoded_data, is_encode = encoder(note, event_type)
                if is_encode:
                    midi_mat[time_ind][pitch_index] = encoded_data
        return midi_mat

    def from_note_seq(self, note_sequence, ticks, pitches):
        # reset
        self.clear()

        # firstly make matrix of note events
        # coord transform
        coordTransform = PianorollNoteSeqTransform(ticks, pitches)
        note_event_mat = self._makeNoteEventMatrixFromNoteSeq(note_sequence, coordTransform)

        # store created data
        self.transform = coordTransform
        self.matrix = note_event_mat
        self.encoded_matrix = note_event_mat
        self.note_seq = note_sequence
        return self
