import os

def default_loader(filepath):
        print("Using default data loader.")
        print("Load following files.")
        print(filepath)
        # data conversion
        # filter data
        # return 'converted data', 'filter condition'
        print("But no data is loaded with default loader.")
        return None, True

class DataProvider:
    def __init__(self):
        self.dataset = []

    def load_datasets(self, datasets_dir, extension, loader = default_loader, verbose = False):
        def search_files_recursively(datasets_dir, extension):
            import fnmatch
            import os
            matches = []
            for root, dirnames, filenames in os.walk(datasets_dir):
                for filename in fnmatch.filter(filenames, '*.{}'.format(extension)):
                    matches.append(os.path.join(root, filename))
            return matches

        new_dataset = []
        for filepath in search_files_recursively(datasets_dir, extension):
            loaded_data, is_ok = loader(filepath)
            if is_ok and loaded_data is not None:
                new_dataset.append(loaded_data)
                if verbose: print("dataset file loaded: {}".format(filepath))

        print "{} additional files are loaded.".format(len(new_dataset))
        self.dataset.extend(new_dataset)

        print "{} files are loaded in total.".format(len(self.dataset))
        return self

    def get_dataset(self):
        return self.dataset

    def get_data(self, index):
        return self.dataset[index]
