
import os, sys
import requests

#---------------------------------------------------------------------#
# https://developers.google.com/drive/api/v3/quickstart/python
# quick_start.py example
'''
from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

# Setup the Drive v3 API
SCOPES = 'https://www.googleapis.com/auth/drive'
store = file.Storage('credentials.json') # set path where to save new credentials
creds = store.get()

if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('drive', 'v3', http=creds.authorize(Http()))

# Call the Drive v3 API
results = service.files().list(
    pageSize=10, fields="nextPageToken, files(id, name)").execute()
items = results.get('files', [])
if not items:
    print('No files found.')
else:
    print('Files:')
    for item in items:
        print('{0} ({1})'.format(item['name'], item['id']))
'''



#-------------------------------------------------------------------#
# uploading files with google drive API
#
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from apiclient.http import MediaFileUpload

class GoogleDrive:

    # download google drive *without Google Drive API*
    # Reference:
    # https://stackoverflow.com/questions/38511444/python-download-files-from-google-drive-using-url
    #
    def download_file_from_google_drive(self, google_drive_id, destination_path):

        def get_confirm_token(response):
            for key, value in response.cookies.items():
                if key.startswith('download_warning'):
                    return value
            return None

        def save_response_content(response, destination):
            CHUNK_SIZE = 32768
            with open(destination, "wb") as f:
                for chunk in response.iter_content(CHUNK_SIZE):
                    if chunk:
                        f.write(chunk)

        URL = "https://docs.google.com/uc?export=download"

        print("Download file from {}, id={}".format(URL, google_drive_id))

        session = requests.Session()
        response = session.get(URL, params = { 'id' : google_drive_id }, stream = True)
        token = get_confirm_token(response)

        if token:
            params = { 'id' : google_drive_id, 'confirm' : token }
            response = session.get(URL, params = params, stream = True)

        print(response)
        save_response_content(response, destination_path)

    #---------------------------------------------------------------------------#
    # Google drive API wrappers
    #

    def __init__(self, input_client_secret_path, output_credentials_path):
        # Setup the Drive v3 API
        SCOPES = 'https://www.googleapis.com/auth/drive' # full permission
        store = file.Storage(output_credentials_path) # set path where to save new 'credentials.json'
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(input_client_secret_path, SCOPES) # 'client_secret.json'
            creds = tools.run_flow(flow, store)
        drive_service = build('drive', 'v3', http=creds.authorize(Http()))
        self.drive_service = drive_service

    def listup_files_in_google_drive(self, num_items=10):
        # Call the Drive v3 API
        results = self.drive_service.files().list(
            pageSize=num_items).execute()
        return results

    def upload_file_to_google_drive(self,local_filepath, folder_id):
        # See 'Work with Folders':
        # https://developers.google.com/drive/api/v3/folder
        filename = os.path.basename(local_filepath)
        file_metadata = {'name': filename, 'parents': [ folder_id ]}
        media = MediaFileUpload(local_filepath,
                                mimetype='application/octet-stream')
        file = self.drive_service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print('File ID: {}'.format(file.get('id')))
