# Utilities for music machine learning

### dependency

+ ml_midi
    + pretty_midi, mido
    + music21 (todo: to be removed in the future)


+ ml_audio (todo)


+ ml_misc
    + google-api-python-client
    + oauth2client

### Development environment

+ Jupyter notebook

### How to use
```
# if use ml_music on jupyter notebook
%matplotlib inline

# Add path to ml_music directory
import sys
sys.path.append("../ml_music_tools") # ml_midi

import ml_music as ml
```



### common functions

+ ml.plot


### ml_midi module
midi preprocessing tools for ML.

classes:

+ ml.midi.NoteSequence
    + ml.midi.Note
    + ml.midi.TimeSignature
    + ml.midi.Tempo
    + ml.midi.Quantization
+ ml.midi.Pianoroll

functions:

+ ml.midi.check_midifile

### ml_audio
audio preprocessing tools for ML.

todo


### ml_misc module
Some utility classes for ML.

classes:

+ ml.util.DataProvider
+ ml.util.GoogleDrive
